package net.thumbtack.klimov.practice2.list;

import org.junit.Test;

import static org.junit.Assert.*;

public class DoublyLinkedListTest {

    @Test
    public void testAdd() throws Exception {
        // arrage
        DoublyLinkedList list = new DoublyLinkedList();

        // act
        list.add(1);

        // assert
        assertEquals(1, list.head.getValue());

    }

    @Test
    public void testIsEmptyForEmpty() throws Exception {
        // arrage
        DoublyLinkedList list = new DoublyLinkedList();

        // act

        // assert
        assertEquals(true, list.isEmpty());
    }

    @Test
    public void testIsEmptyForNoEmpty() throws Exception {
        // arrage
        DoublyLinkedList list = new DoublyLinkedList();

        // act
        list.add(1);

        // assert
        assertEquals(false, list.isEmpty());
    }
}