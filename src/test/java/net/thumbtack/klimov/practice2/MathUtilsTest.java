package net.thumbtack.klimov.practice2;

import net.thumbtack.klimov.practice2.MathUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class MathUtilsTest {

    @Test
    public void test5Plus5Equals10() throws Exception {
        // arrange
        int a = 5, b = 5;

        // act
        int actual = MathUtils.sum(a, b);

        // assert
        assertEquals(10, actual);
    }
}