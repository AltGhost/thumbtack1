package net.thumbtack.klimov.practice3;

import net.thumbtack.klimov.practice2.MathUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStringsss {

    @Test
    public void testStringLength() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.length();

        // assert
        assertEquals(12, actual);
    }
    @Test
    public void testStringCharAt() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        char actual = s.charAt(2);

        // assert
        assertEquals('l', actual);
    }
    @Test
    public void testStringIndexOf1() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf('l');

        // assert
        assertEquals(2, actual);
    }
    @Test
    public void testStringIndexOf2() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf('l',3);

        // assert
        assertEquals(3, actual);
    }
    @Test
    public void testStringIndexOf3() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf("ll",2);

        // assert
        assertEquals(2, actual);
    }
    @Test
    public void testStringIndexOf4() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf("ll",3);

        // assert
        assertEquals(-1, actual);
    }
    @Test
    public void testStringLastIndexOf1() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf('o');

        // assert
        assertEquals(7, actual);
    }
    @Test
    public void testStringLastIndexOf2() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf('o', 7);

        // assert
        assertEquals(-1, actual);
    }
    @Test
    public void testStringLastIndexOf3() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf("or");

        // assert
        assertEquals(7, actual);
    }
    @Test
    public void testStringLastIndexOf4() throws Exception {
        // arrange
        String s = "Hello World!";

        // act
        int actual = s.indexOf("or", 7);

        // assert
        assertEquals(-1, actual);
    }
    @Test
    public void testStringEqualsTrue() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "Hello World!";

        // act
        boolean actual = s1.equals(s2);

        // assert
        assertEquals(true, actual);
    }
    @Test
    public void testStringEqualsFalse() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "Hello!";

        // act
        boolean actual = s1.equals(s2);

        // assert
        assertEquals(false, actual);
    }
    @Test
    public void testStringEqualsIgnoreCase() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "hello world!";

        // act
        boolean actual = s1.equalsIgnoreCase(s2);

        // assert
        assertEquals(true, actual);
    }
    @Test
    public void testStringCompareTo1() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "Hello World!";

        // act
        int actual = s1.compareTo(s2);

        // assert
        assertEquals(0, actual);
    }
    @Test
    public void testStringCompareTo2() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "hello world!";

        // act
        int actual = s1.compareTo(s2);

        // assert
        assertEquals(-32, actual);
    }
    @Test
    public void testStringCompareToIgnoreCase() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "hello world!";

        // act
        int actual = s1.compareToIgnoreCase(s2);

        // assert
        assertEquals(0, actual);
    }
    @Test
    public void testStringConcat1() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "hello";

        // act
        String actual = s1 + s2;

        // assert
        assertEquals("Hello World!hello", actual);
    }
    @Test
    public void testStringConcat2() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String s2 = "hello";

        // act
        String actual = s1.concat(s2);

        // assert
        assertEquals("Hello World!hello", actual);
    }
    @Test
    public void testStringEndsWith() throws Exception {
        // arrange
        String s1 = "Hello World!";

        // act
        boolean actual = s1.endsWith("d!");

        // assert
        assertEquals(true, actual);
    }
    @Test
    public void testStringSubstring() throws Exception {
        // arrange
        String s1 = "Hello World!";

        // act
        String actual = s1.substring(0, 5);

        // assert
        assertEquals("Hello", actual);
    }
    @Test
    public void testStringGetBytes() throws Exception {
        // arrange
        String s1 = "Hello World!";

        // act

        // assert
    }
    @Test
    public void testStringGetChars() throws Exception {
        // arrange
        String s1 = "Hello World!";

        // act

        // assert
    }
    @Test
    public void testStringReplace() throws Exception {
        // arrange
        String s1 = "Hello World!";

        // act
        String actual = s1.replace('l', 'k');

        // assert
        assertEquals("Hekko Workd!", actual);
    }
    @Test
    public void testStringReplaceFirst() throws Exception {
        // arrange
        String s1 = "Hello World!";

        // act
        String actual = s1.replaceFirst("He", "Re");

        // assert
        assertEquals("Rello World!", actual);
    }
    @Test
    public void testStringSplit() throws Exception {
        // arrange
        String s1 = "Hello World!";
        String [] ss = {"Hello", "World!"};
        // act
        String actual[] = s1.split(" ");

        // assert
        assertEquals(ss, actual);
    }
    @Test
    public void testStringTrim() throws Exception {
        // arrange
        String s1 = "    Hello World! ";

        // act
        String actual = s1.trim();

        // assert
        assertEquals("Hello World!", actual);
    }
    @Test
    public void testStringValueOf() throws Exception {
        // arrange
        char c [] = {'a', 'b', 'c', 'd', 'e', 'f','g' };

        // act
        String actual = String.valueOf(c);

        // assert
        assertEquals("abcdefg", actual);
    }
    @Test
    public void testStringFormat() throws Exception {
        // arrange

        // act

        // assert
    }
}
