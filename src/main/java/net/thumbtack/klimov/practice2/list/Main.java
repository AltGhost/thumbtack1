package net.thumbtack.klimov.practice2.list;


public class Main {
    public static void main(String[] args) {
        DoublyLinkedList list = new DoublyLinkedList();
        DoublyLinkedList list1 = new DoublyLinkedList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.printAll();
        System.out.println(list.isEmpty());
        System.out.println(list1.isEmpty());
        System.out.println(list.length);
        list.head();
        list.last();
        list.tail();
    }
}
