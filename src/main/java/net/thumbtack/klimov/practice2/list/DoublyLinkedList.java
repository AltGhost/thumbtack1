package net.thumbtack.klimov.practice2.list;


public class DoublyLinkedList {

    MyNode head;
    MyNode tail;
    int length;

    public void add(int value) {
        MyNode newNode = new MyNode(tail, null, value);
        if (tail != null)
            tail.setNext(newNode);

        tail = newNode;
        if (head == null)
            head = tail;
        length += 1;
    }

    public void delete(int position) {
        if (position > length)
            System.out.println("position > length of list");
        else {

        }
    }

    public void head() {
        System.out.println(head.getValue());
    }

    public void last() {
        System.out.println(tail.getValue());
    }

    public void tail() {
        MyNode temp = head;
        while (temp != null) {
            temp = temp.getNext();
            if (temp == null)
                break;
            System.out.print(temp.getValue());
        }
    }

    public void init() {

    }

    public void printAll() {
        MyNode temp = head;
        while (temp != null) {
            System.out.println(temp.getValue());
            temp = temp.getNext();
        }
    }

    public boolean isEmpty() {
        if (length == 0)
            return true;
        else return false;
    }

}
