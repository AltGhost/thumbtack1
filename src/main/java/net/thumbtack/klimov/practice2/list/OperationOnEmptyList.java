package net.thumbtack.klimov.practice2.list;

public class OperationOnEmptyList extends RuntimeException {
    public OperationOnEmptyList(String s) {
        super(s);
    }
}
