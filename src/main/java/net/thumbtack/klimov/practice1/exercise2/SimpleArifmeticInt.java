package net.thumbtack.klimov.practice1.exercise2;


public class SimpleArifmeticInt {
    public static void main(String[] args) {
        int a = 1000;
        int b = 150;
        System.out.println(a + b);
        System.out.println(a * b);
        System.out.println(a / b);
        System.out.println(a % b);

        System.out.println(a > b);
        System.out.println(a < b);
    }
}
