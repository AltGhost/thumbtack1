package net.thumbtack.klimov.practice1.exercise4;


public class SimplePointInRectangle {


    public static void main(String[] args) {
        // coordinats of Rectangle
        int x1 = 1, y1 = 1;
        int x2 = 5, y2 = 5;

        // coordinats of Point
        int x3 = 2, y3 = 3;

        if (x3 > x1 && x3 < x2 && y3 > y1 && y3 < y2) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
}