package net.thumbtack.klimov.practice1.exercise10;

public class CircleFactory {

    private int counter = 0;

    public void createCircle() {
        counter += 1;
        new Circle();
    }

    public int getCounter() {
        return counter;
    }

    public static void main(String[] args) {
        CircleFactory box = new CircleFactory();
        box.createCircle();
        box.createCircle();
        box.createCircle();
        System.out.println(box.getCounter());
    }
}

class Circle {
    //some code needs to be here
}