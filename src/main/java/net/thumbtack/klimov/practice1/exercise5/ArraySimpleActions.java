package net.thumbtack.klimov.practice1.exercise5;

public class ArraySimpleActions {
    public static void main(String[] args) {
        int arr[] = {4, 8, 14, 45, 77, 43, 66, 686, 32, 33};
        int sum = arr[0];
        int pro = arr[0];
        int min = arr[0];
        int max = arr[0];

        for (int i = 1; i < arr.length; i++) {
            sum += arr[i];
            pro *= arr[i];
            if (arr[i] > max) {
                max = arr[i];
            }
            if (arr[i] < min) {
                min = arr[i];
            }
        }

        double ave = sum / (double) arr.length;

        System.out.println(sum);
        System.out.println(pro);
        System.out.println(min);
        System.out.println(max);
        System.out.println(ave);
    }
}
