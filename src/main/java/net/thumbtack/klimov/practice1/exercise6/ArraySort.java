package net.thumbtack.klimov.practice1.exercise6;


public class ArraySort {
    public static void main(String[] args) {
        int arr[] = {1, 1, 1, 1, 1};

        // flags
        boolean positive = (arr[0] <= arr[arr.length - 1]);

        for (int i = 0; i < arr.length - 1; i++) {
            if (positive) {
                if (arr[i] > arr[i + 1]) {
                    System.out.println("array is not sorted");
                    return;
                }
            } else {
                if (arr[i] < arr[i + 1]) {
                    System.out.println("array is not sorted");
                    return;
                }
            }
        }
        if (positive) {
            System.out.println("array is sorted by growing");
        } else {
            System.out.println("array is sorted by descending");
        }
    }
}
