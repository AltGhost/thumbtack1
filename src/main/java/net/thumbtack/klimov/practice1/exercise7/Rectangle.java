package net.thumbtack.klimov.practice1.exercise7;

import net.thumbtack.klimov.practice1.Point2D;

public class Rectangle {

    private int x1;
    private int y1;
    private int x2;
    private int y2;

    public Rectangle(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Rectangle(int x2, int y2) {
        this.x2 = x2;
        this.y2 = y2;
    }

    public Rectangle() {
        this.x2 = 1;
        this.y2 = 1;
    }

    public void printCordinates() {
        System.out.println("x1: " + x1 + ", y1: " + y1 + ", x2: " + x2 + ", y2: " + y2);
    }

    public void move(int dx, int dy) {
        x1 += dx;
        y1 += dy;
        x2 += dx;
        y2 += dy;
    }

    public void reduce(int nx, int ny) {
        x2 = (x2 - x1) / nx + x1;
        y2 = (y2 - y1) / ny + y1;
    }

    public int square() {
        return (x2 - x1) * (y2 - y1);
    }

    public boolean pointIn(int px, int py) {
        return (x1 < px && px < x2 && y1 < py && py < y2);
    }

    public boolean pointIn(Point2D point) {
        return (x1 < point.getX() && point.getX() < x2 && y1 < point.getY() && point.getY() < y2);
    }

    public boolean crossIn(Rectangle a) {
        if (a.pointIn(x1, y1) || a.pointIn(x2, y2) || a.pointIn(x1, y2) || a.pointIn(x2, y1)) {
            return true;
        } else if (x1 < a.getX1() && a.getX1() < x2 && a.getY1() < y1 && y2 < a.getY2() ||
                a.getX1() < x1 && x1 < a.getX2() && y1 < a.getY1() && y1 < a.getY2()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean into(Rectangle a) {
        return (x1 < a.x1 && y1 < a.y1 && a.x2 < x2 && a.y2 < y2 ||
                a.x1 < x1 && a.y1 < y1 && x2 < a.x2 && y2 < a.y2);
    }

    public Rectangle large(int n) {
        return new Rectangle(x1, y1, (x2 - x1) * n + x1, (y2 - y1) * n + y1);
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rectangle rectangle = (Rectangle) o;

        if (x1 != rectangle.x1) return false;
        if (y1 != rectangle.y1) return false;
        if (x2 != rectangle.x2) return false;
        return y2 == rectangle.y2;

    }

    @Override
    public int hashCode() {
        int result = x1;
        result = 31 * result + y1;
        result = 31 * result + x2;
        result = 31 * result + y2;
        return result;
    }

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(5, 5, 15, 15);
        Rectangle r2 = new Rectangle(10, 10);
        Rectangle r3 = new Rectangle();
        r3.printCordinates();
        r3.move(5, 10);
        r3.printCordinates();

        r1.reduce(5, 5);
        r1.printCordinates();
        System.out.println(r1.square());
        System.out.println(r2.square());

        Rectangle r4 = new Rectangle(4, 3, 16, 15);
        Rectangle r5 = new Rectangle(1, 2, 7, 8);

        System.out.println("pointIN: " + r4.pointIn(3, 7));
        System.out.println("pointIN: " + r4.pointIn(7, 9));

        System.out.println("Point2D in: " + r4.pointIn(new Point2D(5, 6)));
        System.out.println("crossIN: " + r4.crossIn(r5));

        Rectangle r6 = new Rectangle(2, 2, 10, 10);
        Rectangle r7 = new Rectangle(4, 4, 8, 8);

        System.out.println("Into: " + r4.into(r5));
        System.out.println("Into: " + r6.into(r7));

        r6.large(3).printCordinates();

        Rectangle r8 = new Rectangle(4, 4, 8, 8);
        System.out.println(r6.equals(r7));
        System.out.println(r8.equals(r7));
    }
}