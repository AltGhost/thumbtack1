package net.thumbtack.klimov.practice1.exercise3;


public class SimpleArifmeticDouble {
    public static void main(String[] args) {
        double a = 15.54;
        double b = 565.72;
        System.out.println(a + b);
        System.out.println(a * b);
        System.out.println(a > b);
        System.out.println(a < b);
    }
}
