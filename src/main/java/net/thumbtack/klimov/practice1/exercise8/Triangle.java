package net.thumbtack.klimov.practice1.exercise8;

import java.lang.Math;
import net.thumbtack.klimov.practice1.Point2D;

public class Triangle {

    private int x1, y1;
    private int x2, y2;
    private int x3, y3;

    public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
    }

    public void printCorditates() {
        System.out.format("The coordinates of Triangle: (x1: %d, y1: %d); (x2: %d, y2: %d); (x3: %d, y3: %d)%n",
                x1, y1, x2, y2, x3, y3);
    }

    public void move(int dx, int dy) {
        x1 += dx;
        y1 += dy;
        x2 += dx;
        y2 += dy;
        x3 += dx;
        y3 += dy;
    }

    public double square() {
        double a = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        double b = Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
        double c = Math.sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));

        return 0.25 * Math.sqrt((a + b + c) * (a + b - c) * (a + c - b) * (b + c - a));
    }

    public boolean pointIn(int x, int y) {
        int a = (x1 - x) * (y2 - y1) - (x2 - x1) * (y1 - y);
        int b = (x2 - x) * (y3 - y2) - (x3 - x2) * (y2 - y);
        int c = (x3 - x) * (y1 - y3) - (x1 - x3) * (y3 - y);

        if ((a <= 0 && b <= 0 && c <= 0) || (a >= 0 && b >= 0 && c >= 0))
            return true;
        else
            return false;
    }

    public boolean pointIn(Point2D point) {
        int a = (x1 - point.getX()) * (y2 - y1) - (x2 - x1) * (y1 - point.getY());
        int b = (x2 - point.getX()) * (y3 - y2) - (x3 - x2) * (y2 - point.getY());
        int c = (x3 - point.getX()) * (y1 - y3) - (x1 - x3) * (y3 - point.getY());

        if ((a <= 0 && b <= 0 && c <= 0) || (a >= 0 && b >= 0 && c >= 0))
            return true;
        else
            return false;
    }

    public boolean equilateral() {
        double a = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        double b = Math.sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));
        double c = Math.sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));
        if (a == b && a == c && c == b)
            return true;
        else
            return false;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getX3() {
        return x3;
    }

    public int getY3() {
        return y3;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public void setX3(int x3) {
        this.x3 = x3;
    }

    public void setY3(int y3) {
        this.y3 = y3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Triangle triangle = (Triangle) o;

        if (x1 != triangle.x1) return false;
        if (y1 != triangle.y1) return false;
        if (x2 != triangle.x2) return false;
        if (y2 != triangle.y2) return false;
        if (x3 != triangle.x3) return false;
        return y3 == triangle.y3;

    }

    @Override
    public int hashCode() {
        int result = x1;
        result = 31 * result + y1;
        result = 31 * result + x2;
        result = 31 * result + y2;
        result = 31 * result + x3;
        result = 31 * result + y3;
        return result;
    }

    public static void main(String[] args) {

        Triangle t1 = new Triangle(1, 1, 3, 4, 5, 1);
        t1.printCorditates();
        System.out.println(t1.square());
        t1.move(-1, -1);
        t1.printCorditates();
        System.out.println(t1.square());
        System.out.println(t1.pointIn(-1, -1));
        System.out.println(t1.pointIn(1, 1));
        System.out.println(t1.pointIn(new Point2D(2, 2)));
        System.out.println(t1.pointIn(new Point2D(-2, -2)));

        Triangle t2 = new Triangle(-3, 0, 0, 3, 0, -3);

        System.out.println(t1.equilateral());
        System.out.println(t2.equilateral());
    }
}