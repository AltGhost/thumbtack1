package net.thumbtack.klimov.practice1.exercise9;

import java.lang.Math;
import net.thumbtack.klimov.practice1.Point2D;

public class Circle {

    int x, y, r;

    public Circle(int x, int y, int r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public void printCordinates() {
        System.out.format("The coordinates of Circle: (x: %d, y: %d) r: %d%n", x, y, r);
    }

    public void move(int dx, int dy) {
        x += dx;
        y += dy;
    }

    public double square() {
        return Math.PI * r * r;
    }

    public double length() {
        return 2 * Math.PI * r;
    }

    public boolean pointIn(int px, int py) {
        double len = Math.sqrt((x - px) * (x - px) + (y - py) * (y - py));
        return (len <= r);
    }

    public boolean pointIn(Point2D point) {
        double len = Math.sqrt((x - point.getX()) * (x - point.getX()) + (y - point.getY()) * (y - point.getY()));
        return (len <= r);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getR() {
        return r;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setR(int r) {
        this.r = r;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Circle circle = (Circle) o;

        if (x != circle.x) return false;
        if (y != circle.y) return false;
        return r == circle.r;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + r;
        return result;
    }

    public static void main(String[] args) {

        Circle c1 = new Circle(5, 5, 5);
        c1.printCordinates();
        c1.move(5, 5);
        c1.printCordinates();
        System.out.println(c1.square());
        System.out.println(c1.length());
        System.out.println(c1.pointIn(6, 6));
        System.out.println(c1.pointIn(7, 7));

        System.out.println(c1.pointIn(new Point2D(8, 8)));
        System.out.println(c1.pointIn(new Point2D(2, 2)));

    }
}