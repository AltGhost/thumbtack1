package net.thumbtack.klimov.practice3;


import net.thumbtack.klimov.practice1.Point2D;

public class Point3D extends Point2D {

    private int z;

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }
}
