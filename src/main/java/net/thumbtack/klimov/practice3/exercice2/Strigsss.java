package net.thumbtack.klimov.practice3.exercice2;

public class Strigsss {
    public static void main(String[] args) {
        String s = "Hello World!";
        String s2 = "Hello ";
        String s3 = "hello world!";
        String s4 = "Hello World!";
        String s6 = " Hello World! ";
        System.out.println(s.length());
        System.out.println(s.charAt(2));
        System.out.println(s.indexOf('l'));
        System.out.println(s.indexOf('l',3));
        System.out.println(s.indexOf("ll"));
        System.out.println(s.indexOf("ll", 3));
        System.out.println(s.lastIndexOf('o'));
        System.out.println(s.lastIndexOf('o', 7));
        System.out.println(s.lastIndexOf("or"));
        System.out.println(s.lastIndexOf("or", 7));
        System.out.println(s.equals(s2));
        System.out.println(s.equalsIgnoreCase(s3));
        System.out.println(s.compareTo(s2));
        System.out.println(s.compareTo(s4));
        System.out.println(s.compareTo(s3));
        System.out.println(s.compareToIgnoreCase(s3));
        System.out.println(s+s2);
        System.out.println(s.concat(s2));
        System.out.println(s.startsWith("he"));
        System.out.println(s.endsWith("ld!"));
        System.out.println(s.substring(0, 5));
        System.out.println(s.getBytes());
        char a[] = new char[5];
        s.getChars(0,5,a,0);
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]);
        }
        System.out.println();
        System.out.println(s2.replace('l', 'k'));
        System.out.println(s2.replaceFirst("He", "Re"));
        String[] s5 = s.split(" ");
        System.out.println(s5[0] + s5[1]);
        System.out.println(s6);
        System.out.println(s6.trim());
        System.out.println(String.valueOf(a));
//        System.out.println(String.format(s, ));
    }
}
