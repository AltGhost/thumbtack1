package net.thumbtack.klimov.practice3.exercice5;


import java.math.BigDecimal;
import java.math.BigInteger;

public class BigArifmetical {
    public static void main(String[] args) {
        BigInteger a = new BigInteger("8888888888888888888888888");
        BigInteger b = new BigInteger("1111111111111111111111111");
        System.out.println(a.add(b));
        System.out.println(a.divide(b));
        System.out.println(a.add(b.negate()));

        BigDecimal d = new BigDecimal("675757575757575757575757.34345345");
        BigDecimal e = new BigDecimal("344353213475673453456256.2234455");
        System.out.println(d.multiply(e));
    }
}
