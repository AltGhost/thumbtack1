package net.thumbtack.klimov.practice3.exercice6;

public class Main {
    public static void main(String[] args) {
        Rectangle3D r1 = new Rectangle3D(1, 1, 2, 2, 5);
        Rectangle3D r2 = new Rectangle3D(4, 4, 4);
        Rectangle3D r3 = new Rectangle3D();
        r1.printCordinates();
        r2.printCordinates();
        r3.printCordinates();
        r1.move(1, 1);
        r1.printCordinates();
        r2.reduce(2, 2);
        r2.printCordinates();
        System.out.println(r2.square());
        System.out.println(r2.volume());
        Rectangle3D r4 = new Rectangle3D(1, 1, 4, 4, 5);
        System.out.println(r4.pointIn(2, 2, 2));
    }
}
