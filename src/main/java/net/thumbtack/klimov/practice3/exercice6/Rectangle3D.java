package net.thumbtack.klimov.practice3.exercice6;


import net.thumbtack.klimov.practice1.exercise7.Rectangle;

public class Rectangle3D extends Rectangle {

    // need z coordinate !!!
    private int h;

    public Rectangle3D(int x1, int y1, int x2, int y2, int h) {
        setH(h);
        setX1(x1);
        setY1(y1);
        setX2(x2);
        setY2(y2);
    }

    public Rectangle3D(int x2, int y2, int h) {
        setX1(0);
        setY1(0);
        setX2(x2);
        setY2(y2);
        setH(h);
    }

    public Rectangle3D() {
        setX1(0);
        setY1(0);
        setX2(1);
        setY2(1);
        setH(1);
    }

    public void printCordinates() {
        System.out.println("x1: " + getX1() + ", y1: " + getY1() +
                ", x2: " + getX2() + ", y2: " + getY2() + ", h = " + getH());
    }

    public int volume() {
        return square() * h;
    }

    public boolean pointIn(int px, int py, int pz) {
//        without z coordinate (0,h)
        return (getX1() < px && px < getX2() && getY1() < py && py < getY2() && 0 < pz && pz < getH());
    }

    public int getH() {
        return h;
    }

    public void setH(int h) {
        this.h = h;
    }
}
