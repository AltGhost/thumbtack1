package net.thumbtack.klimov.practice3.exercice4;


public class IntDouble {
    public static void main(String[] args) {
        int a = 5;
        double b = 10.5;
        Integer aa = new Integer(a);
        Double bb = new Double(b);
        int a2 = aa;
        double b2 = bb;
        System.out.println(a2);
        System.out.println(b2);
    }
}
